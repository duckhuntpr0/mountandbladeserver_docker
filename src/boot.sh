#!/bin/sh
# /.boot.sh
#bootscript for Mount & Blade: Warband server

#try to keep docker from tinying the tmux
#rather scale down to 'smallest client'
#stty cols 180 rows 60


#cd /
#if [ ! -f /etc/passwd ] && [ ! -d /home/mysticbbs ]
#then
	#echo "Extracting /.system.tar.gz"
	#tar xvzf --keep-directory-symlink --overwrite --recursive-unlink --unlink-first --same-owner -same-order --atime-preserve /.system.tar.gz /
	#chown -R mysticbbs:mysticbbs /home/mysticbbs
#fi

export PS1="$(whoami)@$(hostname) $ "

exec su warbandserver -c /home/warbandserver/.start.sh
#[ "$id" = 0 ] && su -l mysticbbs $0 "$@"


# 'tmux -2 -u' .. '-u' is needed for ANSI text in e.g 'mystic -cfg' rendering corretly(?)
