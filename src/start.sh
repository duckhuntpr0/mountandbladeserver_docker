#/usr/bin/bash
# Mount & Blade Warband  start script for user warbandserver
# /home/warbandserver/.start.sh

export OLDPWD=$HOME
export EDITOR=nano
#export WINEPREFIX=$HOME/.wineprefix


# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

### START TMUX SESSION ###
cd ~
tmux -u -2 -L tmux-warband -f ~/.tmux.conf -2 -u \
  new-session -s 'warbandsrv' -n 'Warband Server Dashboard' "printf '\033]2;Linux Shell\033\\ ' && sleep 0 && /bin/bash ; read" \; \
  set-option -g history-limit 500 \; \
  split-window -h -p 50 "printf '\033]2;SupervisorD Status\033\\ ' && sleep 1 && supervisord -n -c ~/.config/supervisord.conf ; read" \; \
  split-window -v -p 80 "printf '\033]2;Network Status\033\\ '     && sleep 0 && sudo iftop -n -N -p ; read" \; \
  split-window -v -l 10 "printf '\033]2;System Status\033\\ ' && sleep 0 && top -u warbandserver ; read" \; \
  select-pane -t 0 \; \
  split-window -v -l 10 "printf '\033]2;Warband Server Console\033\\ ' && sleep 2 && /bin/bash -c 'mb_warband_dedicated -r Default_Config.txt -m Viking Conquest' ; read" \; \
  select-pane -t 0 \;

