#!/bin/bash

## THIS SCRIPT IS SIMPLY A WRAPPER FOR THE MOUNT & BLADE SERVER EXE

WBEXE_MD5_SUM=eed614da6182737c0a2ceae334aa481b
WBEXE_CURRENT_MD5_SUM=$(md5sum mbserver/mb_warband_dedicated.exe | cut -f1 -d " ")

if [ "$WBEXE_CURRENT_MD5_SUM" = "$WBEXE_MD5_SUM" ]
then
	cd $HOME/mbserver/
	echo "Starting mb_warband_dedicated.exe in $PWD with arguments: $@"
	WINEPREFIX=$HOME/.mbwineprefix wineconsole --backend=curses $HOME/mbserver/mb_warband_dedicated.exe $@
	MBSERVER_EXIT_CODE=$?
	echo "Server exited.."
	exit MBSERVER_EXIT_CODE
else
	## !! ABORT if 'mb_warband_dedicated.exe' checksum has changed !!
	# it would indicate a tampered with or corrupted executable...
	echo "MD5 CHECKSUM OF EXECUTABLE 'mb_warband_dedicated.exe' FAILED!! Aborting.."
	exit 999
fi


#sleep 2

#WINEPREFIX="~/.wineprefix" wineconsole ./mb_warband_dedicated.exe -r Sample_Battle.txt -m Viking Conquest
#WINEPREFIX="$HOME/.wineprefix" wineconsole --backend=curses $MB_WARBAND_EXE -r Sample_Battle.txt -m $MB_WARBAND_MODULE

