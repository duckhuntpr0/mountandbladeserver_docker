##
# Mount & Blade: Warband Viking Conquest Dedicated Server (v1171 2054) Dockerfile
#
#  _______                     __     __,-,__   ______ __           __         __   ________              __                   __ 
# |   |   |.-----.--.--.-----.|  |_  |  ' '__| |   __ \  |.---.-.--|  |.-----.|__| |  |  |  |.---.-.----.|  |--.---.-.-----.--|  |
# |       ||  _  |  |  |     ||   _| |     __| |   __ <  ||  _  |  _  ||  -__| __  |  |  |  ||  _  |   _||  _  |  _  |     |  _  |
# |__|_|__||_____|_____|__|__||____| |_______| |______/__||___._|_____||_____||__| |________||___._|__|  |_____|___._|__|__|_____|
#                                       |_|                                                                                       
#			 ___ ___ __ __     __                ______                                       __   
#			|   |   |__|  |--.|__|.-----.-----. |      |.-----.-----.-----.--.--.-----.-----.|  |_ 
#			|   |   |  |    < |  ||     |  _  | |   ---||  _  |     |  _  |  |  |  -__|__ --||   _|
#			 \_____/|__|__|__||__||__|__|___  | |______||_____|__|__|__   |_____|_____|_____||____|
#			                            |_____|                        |__|                        
#
#							DOCKER SERVER
#
#	Copyright (C) 2020  DuckHuntPr0
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#	
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#	
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
##
                                                                                                                         


#using current i386/debian:stable-slim ( https://hub.docker.com/layers/debian/library/debian/stable-slim/images/sha256-7dfdce465aeb2e5325ceb854a69b66f2c60bc252d840818e6f50f91c0b016bb6?context=explore )
#FROM ff62647a07d1
FROM i386/debian:stable-slim
MAINTAINER DuckHuntPr0 <https://steamcommunity.com/id/Duck_Hunt_Pro>
LABEL maintainer="https://bitbucket.org/duckhuntpr0/mountandbladeserver_docker" \
version="v1171 2054" \
description="Mount & Blade:Warband Viking Conquest Dedicated Server (v1171 2054) Docker. \
Maintainer of this Dockerfile is NOT affiliated with TaleWorlds Entertainment in any way or fashion." \
license="GNU General Public License <https://www.gnu.org/licenses/>"


## LICENSE





########################
# Setup
########################

#default warband server port
ENV WARBAND_SERVER_PORT 7240
#default warband steam port
ENV WARBAND_SERVER_STEAM_PORT 7241
#default dropbear ssh server port
ENV DROPBEAR_PORT 2222

ENV BUILD_PACKS git wget unzip build-essential flex bison libncurses-dev libz-dev manpages-dev libbsd-dev
ENV RUNTIME_REQUIRED_PACKS tmux python3 python3-pip iftop sudo procps
#these might be nice to have at hand
ENV RUNTIME_OPTIONAL_PACKS nano ddclient dropbear less lynx

## Warband Server
# todo: are there _really_ any region differences ?
ENV REGION_US_MD5 1c3b7a194995f66f20eab965e6acd698
ENV REGION_US_DOWNLOAD http://download.taleworlds.com/mb_warband_viking_conquest_dedicated_1171_2054.zip
ENV REGION_US_DOWNLOAD_ALT https://archive.org/download/mb_warband_viking_conquest_dedicated_1171_2054/mb_warband_viking_conquest_dedicated_1171_2054_US.zip
ENV REGION_EU_MD5 1c3b7a194995f66f20eab965e6acd698
ENV REGION_EU_DOWNLOAD http://download3.taleworlds.com/mb_warband_viking_conquest_dedicated_1171_2054.zip
ENV REGION_EU_DOWNLOAD_ALT https://archive.org/download/mb_warband_viking_conquest_dedicated_1171_2054/mb_warband_viking_conquest_dedicated_1171_2054_EU.zip

## WINE
ENV WINE_DOWNLOAD_GIT_URL git://source.winehq.org/git/wine.git
ENV WINE_DOWNLOAD_GIT_TAG wine-1.8.6
ENV WINE_BUILD_FLAGS --with-curses --disable-tests --without-x \
--without-dbus --without-hal --without-netapi --without-krb5 --without-mingw \
--without-freetype --without-fontconfig --without-gettext \
--without-alsa --without-oss --without-pulse  --without-faudio --without-coreaudio --without-openal \
--without-gsm --without-capi --without-ldap \
--without-glu --without-xrender --without-osmesa --without-opencl --without-opengl --without-sdl \
--without-vulkan --without-xcomposite --without-xinerama --without-xrandr --without-xcursor --without-xshape --without-xinput --without-xinput2 --without-xshape  \
--without-gphoto --without-v4l2 \
--without-sane --without-cups --without-xxf86vm

### DropBear (SSH server)
ENV DROPBEAR_DOWNLOAD_URL https://matt.ucc.asn.au/dropbear/releases/dropbear-2020.80.tar.bz2
#ENV DROPBEAR_DOWNLOAD_GIT_URL git://github.com/mkj/dropbear.git
ENV DROPBRAR_DOWNLOAD_GIT_TAG DROPBEAR_2020.80
#ENV DROPBEAR_MD5_SIG 63527580ca47706b70046f87bb9d14fe 





#########################
##
## Building
##
#########################



### Debian Linux setup
##
##
RUN	echo "STARTING BUILD!!" \
	&& apt-get --assume-yes update \
	&& apt-get --assume-yes upgrade \
	&& apt-get --assume-yes install ${BUILD_PACKS} ${RUNTIME_REQUIRED_PACKS} \
	&& pip3 install supervisor setproctitle  \
	&& DEBIAN_FRONTEND=noninteractive apt-get --assume-yes install ${RUNTIME_OPTIONAL_PACKS} \
	&& unset DEBIAN_FRONTEND \
	&& adduser --quiet --disabled-password --shell /bin/sh --home /home/warbandserver --gecos "Warband Server User" warbandserver \
	&& echo "warbandserver:warband" | chpasswd \
	&& usermod -a -G sudo warbandserver


### dropbear
##
##
#RUN	mkdir /tmp/dropbear \
	#&& cd /tmp/dropbear \
	#&& wget --progress=bar:force:noscroll -nc -O dropbear.tar.bz2 ${DROPBEAR_DOWNLOAD_URL} \
	#&& tar jxf dropbear.tar.bz2 \
	#&& cd 'dropbear-2020.80/' \	
	#&& ./configure \
	#&& make \
	#&& make PROGRAMS="dropbear dbclient dropbearkey dropbearconvert scp" install 



### Get WINE
##
##
RUN	cd /tmp/ \
	&& git clone ${WINE_DOWNLOAD_GIT_URL} --depth 1 --progress --branch ${WINE_DOWNLOAD_GIT_TAG} /tmp/wine-dirs/wine-source


### Build WINE
##
##
RUN 	cd /tmp/wine-dirs/ \
	&& mkdir wine-build \
	&& cd wine-build \
	&& ../wine-source/configure ${WINE_BUILD_FLAGS} \
	&& make -j$(( ($(nproc) / 3)+1 ))

RUN 	cd /tmp/wine-dirs/wine-build \
	&& make install-lib

RUN 	echo "*******************************" \
	&& echo "* Configuring 'warbandserver' user's home folder.." \
	&& echo "*******************************" \
	&& su warbandserver -c "export PATH=$HOME/warbandserver/.local/bin:$PATH" \
	&& su warbandserver -c "export WINEPREFIX=$HOME/.mbwineprefix" \
	&& su warbandserver -c "cd ~" \
	&& su warbandserver -c "mkdir ~/logs" \
	&& su warbandserver -c "mkdir ~/Mount\&Blade\ Warband" \
	&& su warbandserver -c "mkdir ~/docs" \
	&& su warbandserver -c "mkdir ~/bin" \
	&& su warbandserver -c "mkdir -p ~/.config/supervisor-inc/" \
	&& su warbandserver -c "echo 'export WINEPREFIX=$HOME/.mbwineprefix' >> ~/.profile" \
	&& su warbandserver -c "echo 255 > ~/.fresh"


#	&& su warbandserver -c "wineconsole --help"

### get server files
##
##
RUN	cd /tmp/ \
	&& wget --progress=bar:force:noscroll -nc -O '/tmp/server.zip' ${REGION_EU_DOWNLOAD}

	#&& export WGET_EXIT_CODE=$? \
	#&& if [ ${WGET_EXIT_CODE} != 0 ];then \
			#echo ${WGET_EXIT_CODE} && \
			#rm /tmp/server.zip && \
			#echo "Failed at downloading warband server from taleworlds.com.." && \
			#echo "Falling back to downloading the server from archive.org!" && \
			#wget --progress=bar:force:noscroll -nc -O '/tmp/server.zip' ${REGION_EU_DOWNLOAD_ALT} \
		#;else \
			#echo "Could not get warband server files! Aborting" && \
			#exit 255 \
		#;fi

RUN 	su warbandserver -c "unzip /tmp/server.zip -d ~/.mbserver_files" \
	&& su warbandserver -c "ln -s /home/warbandserver/Mount\&Blade\ Warband /home/warbandserver/mbserver" \
	&& su warbandserver -c "mv ~/.mbserver_files/mb_warband_viking_conquest_dedicated_1171_2054/Mount\&Blade\ Warband\ Viking\ Conquest\ Dedicated/* ~/mbserver*"
	
##


### Cleanup
##
##
RUN	rm -rf /tmp/wine-dirs \
	&& rm -rf /tmp/dropbear \
	&& rm -rf /tmp/server.zip \
	&& apt-get --assume-yes remove ${BUILD_PACKS} \
	&& apt-get --assume-yes autoremove \
	&& apt-get --assume-yes autoclean \
	&& apt-get --assume-yes clean


### Set a random password for root for security
## ('root' should _NEVER_ be necessary to remote into)
##
RUN	echo "Setting a randomized password for root user.." \
	&& echo "(it should not be necessary to remote into 'root' account)" \
	&& echo "root:$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 1025 )" | chpasswd 


#todo: optimize redundant chown stuff below!! and do some cleanup of the stuff
COPY --chown=root:root ./src/boot.sh /.boot.sh
COPY --chown=warbandserver:warbandserver ./src/start.sh /home/warbandserver/.start.sh
COPY --chown=warbandserver:warbandserver ./src/tmux.conf /home/warbandserver/.tmux.conf
COPY --chown=warbandserver:warbandserver ./src/supervisord.conf /home/warbandserver/.config/supervisord.conf
COPY --chown=warbandserver:warbandserver ./src/mb_warband_dedicated.sh /home/warbandserver/bin/mb_warband_dedicated
COPY --chown=warbandserver:warbandserver ./src/bash_aliases /home/warbandserver/.bash_aliases
COPY --chown=warbandserver:warbandserver ./src/wbcmd.sh /home/warbandserver/bin/wbcmd
COPY --chown=warbandserver:warbandserver ./Dockerfile /home/warbandserver/docs/
COPY --chown=warbandserver:warbandserver ./LICENSE.TXT /home/warbandserver/docs/LICENSE.TXT
COPY --chown=warbandserver:warbandserver ./README.md /home/warbandserver/docs/README.TXT
COPY --chown=warbandserver:warbandserver ./src/supervisor-inc/* /home/warbandserver/.config/supervisor-inc/

RUN 	chmod +x /.boot.sh \
	&& su warbandserver -c "ln -s ~/mbserver/readme.txt ~/docs/mount_and_blade_dedicated_server_readme.txt" \
	&& chmod +x /home/warbandserver/.start.sh \
	&& chmod +x /home/warbandserver/.bash_aliases \
	&& chmod +x /home/warbandserver/bin/mb_warband_dedicated \
	&& echo "%warbandserver ALL=(root) NOPASSWD: /usr/sbin/iftop" >> /etc/sudoers.d/allow_iftop \
	&& su warbandserver -c "cp ~/mbserver/Sample_Battle.txt ~/mbserver/Default_Config.txt" \
	&& su warbandserver -c "WINEPREFIX=/home/warbandserver/.mbwineprefix wineboot -i" \
	&& chown warbandserver:warbandserver /home/warbandserver/bin/wbcmd \
	&& chmod +x /home/warbandserver/bin/wbcmd




EXPOSE ${DROPBEAR_PORT}/tcp
EXPOSE ${WARBAND_SERVER_PORT}
EXPOSE ${WARBAND_SERVER_STEAM_PORT}

ENTRYPOINT ["/.boot.sh"]
